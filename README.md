# java-ca3

A simple cellular automaton with a GUI, written in Java and JFoenix/JavaFX. 
It can be used, for example, to play Conway's Game of Life and provides a template
for the "Glider", "LW-Spaceship", "Tumbler", "Octagon" and the "Chain".
Of course, one can draw also individual structures by dragging and holding the mouse.

## Setup

Currently, it is recommended to check the source code out and load it in a Java-IDE. 
There are some dependencies, which for example, Intellij IDEA can easily resolve.
The main-class, which then is started due to the IDE is "main.java.de.jo2k.ca3.Main".

## Usage

After starting java-ca3, new cells can be drawn with the left mouse button and the 
map can be moved by holding the right mosue button.
In the burger-menu, one can access the templates and change the size of the map.

**It is strongly recommended to watch the 
[video](https://gitlab.com/jliebers/java-ca3/blob/master/java-ca3.mp4).**