package main.java.de.jo2k.ca3;

import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import main.java.de.jo2k.ca3.Controller.MainController;
import main.java.de.jo2k.ca3.Model.Automaton;
import main.java.de.jo2k.ca3.View.CustomJFXDecorator;

public class Main extends Application {
    @FXMLViewFlowContext
    private ViewFlowContext flowContext;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        Flow flow = new Flow(MainController.class);
        DefaultFlowContainer container = new DefaultFlowContainer();
        flowContext = new ViewFlowContext();
        flowContext.register("Stage", stage);
        flow.createHandler(flowContext).start(container);

        CustomJFXDecorator decorator = new CustomJFXDecorator(stage, container.getView(), true, true, true);
        decorator.setCustomMaximize(true);

        double width = 800;
        double height = 600;
        try {
            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
            width = bounds.getWidth() / 2.5;
            height = bounds.getHeight() / 1.35;
        } catch (Exception e){
            e.printStackTrace();
        }

        Scene scene = new Scene(decorator, width, height);
        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(Main.class.getResource("/main/resources/css/jfoenix-main-demo.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle("JavaFX-CA3");
        stage.show();
    }

    @Override
    public void stop(){
        // stop cycleThread, so it does not keep running after exiting app
        Automaton.getInstance().stopCycleThread();
        System.out.println("Exiting Java-FX-CA3. Bye.");
    }
}
