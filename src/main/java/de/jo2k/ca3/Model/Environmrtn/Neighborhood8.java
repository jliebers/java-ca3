package main.java.de.jo2k.ca3.Model.Environmrtn;

import main.java.de.jo2k.ca3.Model.Cell;

import java.util.ArrayList;
import java.util.Collections;

public class Neighborhood8 {
    public Cell TOPLEFT, TOPMIDDLE, TOPRIGHT, MIDDLELEFT, MIDDLEMIDDLE, MIDDLERIGHT, BOTTOMLEFT, BOTTOMMIDDLE, BOTTOMRIGHT;

    public Neighborhood8(int x, int y, Cell[][] cells) {
        final int sizeX = cells.length-1; // proper index, e.g. size = 10, last index = 9
        final int sizeY = cells[0].length-1;

        if (x == 0 || y == 0 || x == sizeX || y == sizeY) { // special case if requested cell is at a corner of the map
            if (x == 0 && y == 0) { // bottom left corner
                this.TOPLEFT = cells[sizeX][y + 1];
                this.TOPMIDDLE = cells[x][y + 1];
                this.TOPRIGHT = cells[x + 1][y + 1];
                this.MIDDLELEFT = cells[sizeX][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[x + 1][y];
                this.BOTTOMLEFT = cells[sizeX][sizeY];
                this.BOTTOMMIDDLE = cells[x][sizeY];
                this.BOTTOMRIGHT = cells[x + 1][sizeY];
                return;
            } else if (x == 0 && y == sizeY) { // top left corner
                this.TOPLEFT = cells[sizeX][0];
                this.TOPMIDDLE = cells[x][0];
                this.TOPRIGHT = cells[x + 1][0];
                this.MIDDLELEFT = cells[sizeX][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[x + 1][y];
                this.BOTTOMLEFT = cells[sizeX][y - 1];
                this.BOTTOMMIDDLE = cells[x][y - 1];
                this.BOTTOMRIGHT = cells[x + 1][y - 1];
                return;
            } else if (x == sizeX && y == sizeY) { // top right corner
                this.TOPLEFT = cells[x - 1][0];
                this.TOPMIDDLE = cells[x][0];
                this.TOPRIGHT = cells[0][0];
                this.MIDDLELEFT = cells[x - 1][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[0][y];
                this.BOTTOMLEFT = cells[x - 1][y - 1];
                this.BOTTOMMIDDLE = cells[x][y - 1];
                this.BOTTOMRIGHT = cells[0][y - 1];
                return;
            } else if (x == sizeX && y == 0) { // bottom right corner
                this.TOPLEFT = cells[x - 1][y + 1];
                this.TOPMIDDLE = cells[x][y + 1];
                this.TOPRIGHT = cells[0][y + 1];
                this.MIDDLELEFT = cells[x - 1][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[0][y];
                this.BOTTOMLEFT = cells[x - 1][sizeY];
                this.BOTTOMMIDDLE = cells[x][sizeY];
                this.BOTTOMRIGHT = cells[0][sizeY];
                return;
            } else if (y == 0) { // bottom horizontal row, not a corner
                this.TOPLEFT = cells[x - 1][y + 1];
                this.TOPMIDDLE = cells[x][y + 1];
                this.TOPRIGHT = cells[x + 1][y + 1];
                this.MIDDLELEFT = cells[x - 1][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[x + 1][y];
                this.BOTTOMLEFT = cells[x - 1][sizeY];
                this.BOTTOMMIDDLE = cells[x][sizeY];
                this.BOTTOMRIGHT = cells[x + 1][sizeY];
                return;
            } else if (x == 0) { // left vertical row, not a corner
                this.TOPLEFT = cells[sizeX][y + 1];
                this.TOPMIDDLE = cells[x][y + 1];
                this.TOPRIGHT = cells[x + 1][y + 1];
                this.MIDDLELEFT = cells[sizeX][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[x + 1][y];
                this.BOTTOMLEFT = cells[sizeX][y - 1];
                this.BOTTOMMIDDLE = cells[x][y - 1];
                this.BOTTOMRIGHT = cells[x + 1][y - 1];
                return;
            } else if (y == sizeY) { // top horizontal row, not a corner
                this.TOPLEFT = cells[x - 1][0];
                this.TOPMIDDLE = cells[x][0];
                this.TOPRIGHT = cells[x + 1][0];
                this.MIDDLELEFT = cells[x - 1][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[x + 1][y];
                this.BOTTOMLEFT = cells[x - 1][y - 1];
                this.BOTTOMMIDDLE = cells[x][y - 1];
                this.BOTTOMRIGHT = cells[x + 1][y - 1];
                return;
            } else if (x == sizeX) { // right vertical row, not a corner
                this.TOPLEFT = cells[x - 1][y + 1];
                this.TOPMIDDLE = cells[x][y + 1];
                this.TOPRIGHT = cells[0][y + 1];
                this.MIDDLELEFT = cells[x - 1][y];
                this.MIDDLEMIDDLE = cells[x][y];
                this.MIDDLERIGHT = cells[0][y];
                this.BOTTOMLEFT = cells[x - 1][y - 1];
                this.BOTTOMMIDDLE = cells[x][y - 1];
                this.BOTTOMRIGHT = cells[0][y - 1];
                return;
            }
        } else { // cell is not at a corner but in the middle of the map
            this.TOPLEFT = cells[x - 1][y + 1];
            this.TOPMIDDLE = cells[x][y + 1];
            this.TOPRIGHT = cells[x + 1][y + 1];
            this.MIDDLELEFT = cells[x - 1][y];
            this.MIDDLEMIDDLE = cells[x][y];
            this.MIDDLERIGHT = cells[x + 1][y];
            this.BOTTOMLEFT = cells[x - 1][y - 1];
            this.BOTTOMMIDDLE = cells[x][y - 1];
            this.BOTTOMRIGHT = cells[x + 1][y - 1];
            return;
        }
    }

    public ArrayList<Cell> getList(){
        ArrayList<Cell> cellsList = new ArrayList();
        Collections.addAll(cellsList, TOPLEFT, TOPMIDDLE, TOPRIGHT, MIDDLELEFT, MIDDLERIGHT, BOTTOMLEFT, BOTTOMMIDDLE, BOTTOMRIGHT);
        return cellsList;
    }
}
