package main.java.de.jo2k.ca3.Model;



public class Cell {
    // Like a pawn in chess does not need to know its position, a cell does not need to know its coordinates
    // The coordinates are solely managed by the AutomatonState

    private boolean isAlive;

    public Cell(boolean isAlive){
        this.isAlive = isAlive;
    }

    public boolean isAlive(){
        return isAlive;
    }

    void setAlive(boolean alive){
        this.isAlive = alive;
    }

}
