package main.java.de.jo2k.ca3.Model;

import javafx.application.Platform;
import main.java.de.jo2k.ca3.Model.Environmrtn.Neighborhood8;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Observable;
import java.util.function.Function;

import static java.lang.Thread.sleep;

public class Automaton extends Observable {
    private static final int DEFAULTSIZE = 10;
    private static Automaton instance = null;
    LinkedList<AutomatonState> stateList; // iterable List of all AutomatonStates. Last element is current state.
    int automatonSize;

    int currentCycle = 0; // automatons current currentCycle

    Thread cycleThread;
    private boolean isRunning = false;
    private int cycleThreadSleepTime = 100;

    private AutomatonError errorFlag = null;

    AutomatonState firstUserState;

    private Automaton(int size) {
        automatonSize = size;
        stateList = new LinkedList<>();

        stateList.add(initGameOfLife()); // TODO
        signalizeUpdateGUI();
    }

    public static Automaton getInstance() {
        if (instance == null) {
            instance = new Automaton(DEFAULTSIZE);
        }
        return instance;
    }

    public void setSize(int newSize) {
        // since in java we can not copy the list of registered observers we have to reinitialize everything by hand
        instance.automatonSize = newSize;
        instance.stateList = new LinkedList<>();
        stateList.add(initGameOfLife()); // TODO

        instance.currentCycle = 0;
        instance.cycleThread = null;

        signalizeUpdateGUI();
    }

    public AutomatonState getCurrentState() {
        return stateList.getLast();
    }

    /**
     * Dispatch cycle thread.
     */
    public void startCycleThread() {
        if(checkError())
            return;

        // runnable for that thread
        cycleThread = new Thread(() -> {
            while (isRunning()) {
                Platform.runLater(new Runnable() {
                    public void run() {
                        doCycle();
                    }
                });
                try {
                    sleep(cycleThreadSleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return;
        });
        cycleThread.setPriority(Thread.MIN_PRIORITY);
        cycleThread.setDaemon(false);
        cycleThread.start();
    }

    public void stopCycleThread() {
        cycleThread = null;
    }

    public void doCycle() { // alias
        calculateNextState();
    }

    public void calculateNextState() {
        // make backup of first user state
        if (currentCycle == 0) {
            firstUserState = getCurrentState();
        }

        if(getUnitializedCells().size() != 0){
            errorFlag = AutomatonError.UNINIT_CELLS;
            System.err.println("Automaton contains uninitialized cells. Calculation is not possible -> " + Arrays.deepToString(getUnitializedCells().toArray()));
            cycleThread = null;
            signalizeUpdateGUI();
            return;
        }



        stateList.add(calcGameOfLife(getCurrentState()));
        currentCycle += 1;

        errorFlag = null; // Clear errorflag.
        signalizeUpdateGUI();
    }

    /**
     * This method checks, whether any unitialized cells are in the automaton.
     * @return list of coordinates int[x, y] of null cells. Null if none was found.
     */
    public LinkedList<int[]> getUnitializedCells() {
        LinkedList<int[]> nullCellsCoords = new LinkedList<>();

        for(int x = 0; x < getCurrentState().getSize(); x++){
            for(int y = 0; y < getCurrentState().getSize(); y++){
                if(getCurrentState().getCell(x, y) == null){
                    int[] nullCell = {x, y};
                    nullCellsCoords.add(nullCell);
                }
            }
        }

        return nullCellsCoords;
    }

    public void resetToFirstUserstate() {
        if (firstUserState == null)
            return;

        currentCycle = 0;
        stateList.clear();
        stateList.add(firstUserState);

        errorFlag = null;
        signalizeUpdateGUI();
    }

    public void resetToNew() {
        currentCycle = 0;
        stateList.clear();
        stateList.add(initGameOfLife());

        errorFlag = null;
        signalizeUpdateGUI();
    }

    public void restorePreviousState() {
        if (currentCycle == 0)
            return;

        stateList.removeLast();
        currentCycle -= 1;

        signalizeUpdateGUI();
    }

    public AutomatonState initGameOfLife() {
        AutomatonState newState = new AutomatonState(automatonSize);

        for (int x = 0; x < newState.getSize(); x++) { // for each cell
            for (int y = 0; y < newState.getSize(); y++) {
                newState.setCell(x, y, new Cell(false));
            }
        }

        newState.setCell(3, 3, new Cell(true));
        newState.setCell(3, 4, new Cell(true));
        newState.setCell(3, 5, new Cell(true));

        return newState;
    }

    public void drawGOLGlider(int x, int y, AutomatonState as){
        as.setCell(x, y+1, new Cell(true));
        as.setCell(x+1, y, new Cell(true));
        as.setCell(x+1, y-1, new Cell(true));
        as.setCell(x, y-1, new Cell(true));
        as.setCell(x-1,y-1,new Cell(true));
    }


    public void drawGOLLwSpaceship(int x, int y, AutomatonState as){
        as.setCell(x+1, y+2,new Cell(true));
        as.setCell(x+2, y+1,new Cell(true));
        as.setCell(x+2, y,new Cell(true));
        as.setCell(x+2, y-1,new Cell(true));
        as.setCell(x+1,y-1,new Cell(true));
        as.setCell(x, y-1,new Cell(true));
        as.setCell(x-1, y-1,new Cell(true));
        as.setCell(x-2, y,new Cell(true));
        as.setCell(x-2,y+2,new Cell(true));

    }

    public void drawGOLTumbler(int x, int y, AutomatonState as){
        as.setCell(x+3, y+2,new Cell(true));
        as.setCell(x+2, y+1,new Cell(true));
        as.setCell(x+4, y+1,new Cell(true));
        as.setCell(x+1, y,new Cell(true));
        as.setCell(x+2, y,new Cell(true));
        as.setCell(x+2, y-1,new Cell(true));
        as.setCell(x+3, y-1,new Cell(true));
        as.setCell(x+1, y-2,new Cell(true));
        as.setCell(x+2, y-2,new Cell(true));

        as.setCell(x-1, y-2,new Cell(true));
        as.setCell(x-2, y-2,new Cell(true));
        as.setCell(x- 2, y-1,new Cell(true));
        as.setCell(x-3, y-1,new Cell(true));
        as.setCell(x-1, y,new Cell(true));
        as.setCell(x-2, y,new Cell(true));
        as.setCell(x-2, y+1,new Cell(true));
        as.setCell(x-4, y+1,new Cell(true));
        as.setCell(x-3, y+2,new Cell(true));
    }

    public void drawGOLOctagon(int x, int y, AutomatonState as){
        as.setCell(x, y+4,new Cell(true));
        as.setCell(x+1, y+3,new Cell(true));
        as.setCell(x+2, y+2,new Cell(true));
        as.setCell(x+1, y+3,new Cell(true));
        as.setCell(x+3, y,new Cell(true));
        as.setCell(x+2, y-1,new Cell(true));
        as.setCell(x+1, y-2,new Cell(true));
        as.setCell(x, y-3,new Cell(true));
        as.setCell(x-1, y-3,new Cell(true));
        as.setCell(x-2, y-2,new Cell(true));
        as.setCell(x-3, y-1,new Cell(true));
        as.setCell(x-4, y,new Cell(true));
        as.setCell(x-4, y+1,new Cell(true));
        as.setCell(x-3, y+2,new Cell(true));
        as.setCell(x-2, y+3,new Cell(true));
        as.setCell(x-1, y+4, new Cell(true));
        as.setCell(x+3, y+1, new Cell(true));
    }

    public void drawGOLChain(int x, int y, AutomatonState as){
        as.setCell(x, y+3,new Cell(true));
        as.setCell(x+1, y+3,new Cell(true));
        as.setCell(x+1, y+2,new Cell(true));
        as.setCell(x+1, y+1,new Cell(true));
        as.setCell(x+1, y-1,new Cell(true));
        as.setCell(x+1, y-2,new Cell(true));
        as.setCell(x+1, y-3,new Cell(true));
        as.setCell(x, y-3,new Cell(true));
        as.setCell(x-1, y-3,new Cell(true));
        as.setCell(x-1, y-2,new Cell(true));
        as.setCell(x-1,y-1,new Cell(true));
        as.setCell(x-1, y+1,new Cell(true));
        as.setCell(x-1, y+2,new Cell(true));
        as.setCell(x-1, y+3,new Cell(true));
    }




    public AutomatonState calcGameOfLife(AutomatonState currentState) {
        AutomatonState newState = new AutomatonState(automatonSize);

        Function<Neighborhood8, Integer> getLivingNeighbors = (nb) -> (int) nb.getList().stream().filter(Cell::isAlive).count();

        for (int x = 0; x < currentState.getSize(); x++) { // for each cell
            for (int y = 0; y < currentState.getSize(); y++) {
                Cell cCell = currentState.getCell(x, y);
                Neighborhood8 cNeighborhood = currentState.getNeighborhood8(x, y);

                int aliveNeighbors = getLivingNeighbors.apply(cNeighborhood);

                if (aliveNeighbors == 3 && !cCell.isAlive()) {
                    newState.setCell(x, y, new Cell(true)); // Cell is born
                } else if (aliveNeighbors < 2 && cCell.isAlive()) {
                    newState.setCell(x, y, new Cell(false)); // Cell dies due to loneliness
                } else if ((aliveNeighbors == 2 || aliveNeighbors == 3) && cCell.isAlive()) {
                    newState.setCell(x, y, cCell); // Cell keeps on living
                } else if (aliveNeighbors > 3 && cCell.isAlive()) {
                    newState.setCell(x, y, new Cell(false));
                } else {
                    newState.setCell(x, y, new Cell(false)); // dead cells stay dead
                }
            }
        }
        return newState;
    }

    public void trimList(int size) {
        while (stateList.size() > size) {
            stateList.removeFirst();
        }
    }

    public int getSize() {
        return automatonSize;
    }

    public int getCurrentCycle() {
        return currentCycle;
    }

    public boolean isRunning() {
        return cycleThread != null;
    }

    /**
     * Signalizes the GUI to update itself
     */
    public void signalizeUpdateGUI() {
        setChanged();
        notifyObservers();
    }

    /**
     * Sets the speed of the cycle thread. This function is intended to be used by a slider.
     *
     * @param speed in percent (100 = zero sleep, 1 = 1 Hz)
     */
    public void setCycleThreadSpeed(int speed) {
        if (speed < 0 || speed > 100) {
            System.err.println("setCycleThreadSpeed: Wrong speed set (allowed 0-100): " + speed);
            return;
        }

        cycleThreadSleepTime = 1005 - speed*10; // must be at least 1ms under every circumstance!
    }

    public boolean checkError(){
        return errorFlag != null;
    }

    public AutomatonError getErrorFlag(){
        return errorFlag;
    }


}



