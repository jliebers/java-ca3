package main.java.de.jo2k.ca3.Model;

import main.java.de.jo2k.ca3.Model.Environmrtn.Neighborhood8;

public class AutomatonState {
    private Cell[][] cells;

    public AutomatonState(int s){
        cells = new Cell[s][s];
    }

    public Cell getCell(int x, int y){
        return cells[x][y];
    }

    public void setCell(int x, int y, Cell c){
        x = x % cells.length;
        y = y % cells[0].length;

        cells[x][y] = c;
    }

    public Cell[][] getCells(){
        return cells;
    }

    public Neighborhood8 getNeighborhood8(int x, int y){
            return new Neighborhood8(x, y, cells);
    }

    public int getSize(){
        return cells.length;
    }
}
