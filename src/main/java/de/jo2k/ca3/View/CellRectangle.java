package main.java.de.jo2k.ca3.View;

import javafx.animation.*;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import main.java.de.jo2k.ca3.Controller.AutomatonViewController;
import main.java.de.jo2k.ca3.Model.Cell;

public class CellRectangle extends Rectangle {
    int cellX, cellY; // coordinates of the cell within the map

    public CellRectangle(Cell c, int x, int y){
        super(AutomatonViewController.CELL_SIZE, AutomatonViewController.CELL_SIZE);

        this.cellX = x;
        this.cellY = y;

        if(c == null){
            this.setFill(Color.TRANSPARENT);
            return;
        }

        if(c.isAlive()) { // living cell
            this.setFill(Color.BLUE.deriveColor(1, 1, 1, 0.5));
        } else { // dead cell
            this.setFill(Color.BLACK.deriveColor(1, 1, 1, 0.5));
        }

/*
        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                System.out.println("Mouse pressed at x:"+cellX+", y:"+cellY);
                me.consume();
            }
        });
  */
    }


    public int getCellY() {
        return cellY;
    }

    public int getCellX() {
        return cellX;
    }

    public void setCellX(int cellX){
        this.cellX = cellX;
    }

    public void setCellY(int cellY){
        this.cellY = cellY;
    }

    public void blink(Color c){
        this.setFill(c.deriveColor(1, 1, 1, 1));
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(1), evt -> this.setVisible(false)),
                new KeyFrame(Duration.seconds(2), evt -> this.setVisible(true)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
