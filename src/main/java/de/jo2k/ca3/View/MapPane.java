package main.java.de.jo2k.ca3.View;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import main.java.de.jo2k.ca3.Controller.AutomatonViewController;

import java.util.Observable;
import java.util.Observer;

// Courtesy of https://stackoverflow.com/a/29530135

public class MapPane extends Pane {
    DoubleProperty myScale = new SimpleDoubleProperty(1.0);

    public MapPane() {
        setStyle("-fx-background-color: lightgrey;");

        // add scale transform
        scaleXProperty().bind(myScale);
        scaleYProperty().bind(myScale);
    }

    public double getScale() {
        return myScale.get();
    }

    public void setScale(double scale) {
        myScale.set(scale);
    }

    public void setPivot(double x, double y) {
        setTranslateX(getTranslateX() - x);
        setTranslateY(getTranslateY() - y);
    }

    private void add(Node n) {
        this.getChildren().add(n);
    }

    public void drawCellRectangle(CellRectangle cr) {
        cr.setTranslateX(cr.getCellX() * AutomatonViewController.CELL_SIZE);
        cr.setTranslateY(cr.getCellY() * AutomatonViewController.CELL_SIZE);
        this.add(cr);
    }

    public void clear() {
        this.getChildren().clear();
    }

}

/**
 * Mouse drag context used for scene and nodes.
 */
class DragContext {
    double mouseAnchorX;
    double mouseAnchorY;

    double translateAnchorX;
    double translateAnchorY;
}

