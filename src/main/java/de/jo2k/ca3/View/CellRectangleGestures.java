package main.java.de.jo2k.ca3.View;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * Listeners for making the nodes draggable via left mouse button. Considers if parent is zoomed.
 */
public class CellRectangleGestures {

    private DragContext nodeDragContext = new DragContext();

    MapPane canvas;

    public CellRectangleGestures(MapPane canvas) {
        this.canvas = canvas;

    }
    public EventHandler<MouseEvent> getOnMousePressedEventHandler() {
        return onMousePressedEventHandler;
    }

    public EventHandler<MouseEvent> getOnMouseDraggedEventHandler() {
        return onMouseDraggedEventHandler;
    }

    public EventHandler<MouseEvent> getOnMouseEnteredEventHandler() {
        return onMouseEnteredEventHandler;
    }

    private EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {

        public void handle(MouseEvent event) {
            // left mouse button => dragging

            if(!event.isPrimaryButtonDown())
                return;

            nodeDragContext.mouseAnchorX = event.getSceneX();
            nodeDragContext.mouseAnchorY = event.getSceneY();

            CellRectangle cr = (CellRectangle) event.getSource();

            System.out.println(cr.getCellX() + " click");
            System.out.println(cr.getCellY() + " click");

            //nodeDragContext.translateAnchorX = node.getTranslateX();
            //nodeDragContext.translateAnchorY = node.getTranslateY();

        }
    };

    private EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        public void handle(MouseEvent event) {

            // left mouse button => dragging
            if(!event.isPrimaryButtonDown())
                return;

            double scale = canvas.getScale();

            CellRectangle cr = (CellRectangle) event.getSource();

            System.out.println(cr.getCellX());
            System.out.println(cr.getCellY());

            //System.out.println(nodeDragContext.translateAnchorX + ((event.getSceneX() - nodeDragContext.mouseAnchorX) / scale));
            //System.out.println(nodeDragContext.translateAnchorY + ((event.getSceneY() - nodeDragContext.mouseAnchorY) / scale));


        }
    };

    private EventHandler<MouseEvent> onMouseEnteredEventHandler = new EventHandler<MouseEvent>() {
        public void handle(MouseEvent event) {
            CellRectangle cr = (CellRectangle) event.getSource();

            System.out.println(cr.getCellX() +"+ enter");
            System.out.println(cr.getCellY() +"+ enter");

        }
    };
}
