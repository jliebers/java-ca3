package main.java.de.jo2k.ca3.Controller;

import com.jfoenix.controls.*;
import com.jfoenix.controls.JFXPopup.PopupHPosition;
import com.jfoenix.controls.JFXPopup.PopupVPosition;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import main.java.de.jo2k.ca3.Model.Automaton;

import javax.annotation.PostConstruct;

import java.util.Observable;
import java.util.Observer;

import static io.datafx.controller.flow.container.ContainerAnimations.SWIPE_LEFT;

@ViewController(value = "/main/resources/fxml/Main.fxml", title = "Material Design Example")
public final class MainController implements Observer {

    @FXMLViewFlowContext
    private ViewFlowContext context;

    @FXML
    private StackPane root;

    @FXML
    private StackPane titleBurgerContainer;
    @FXML
    private JFXHamburger titleBurger;

    @FXML
    private StackPane optionsBurger;
    @FXML
    private JFXRippler optionsRippler;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private JFXPopup toolbarPopup;

    @FXML
    private JFXDialog dialog;

    @FXML
    private JFXButton acceptButton;

    @FXML
    private Button btn1, btn2, btn3, btn4, btn5;

    @FXML
    private Label lblCycle;

    /**
     * init fxml when loaded.
     */
    @PostConstruct
    public void init() throws Exception {
        // init the title hamburger icon
        drawer.setOnDrawerOpening(e -> {
            final Transition animation = titleBurger.getAnimation();
            animation.setRate(1);
            animation.play();
        });
        drawer.setOnDrawerClosing(e -> {
            final Transition animation = titleBurger.getAnimation();
            animation.setRate(-1);
            animation.play();
        });
        titleBurgerContainer.setOnMouseClicked(e -> {
            if (drawer.isHidden() || drawer.isHiding()) {
                drawer.open();
            } else {
                drawer.close();
            }
        });

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/fxml/MainPopup.fxml"));
        loader.setController(new InputController());
        toolbarPopup = new JFXPopup(loader.load());

        optionsBurger.setOnMouseClicked(e -> toolbarPopup.show(optionsBurger,
                PopupVPosition.TOP,
                PopupHPosition.RIGHT,
                -12,
                15));

        // create the inner flow and content
        context = new ViewFlowContext();
        // set the default controller
        Flow innerFlow = new Flow(AutomatonViewController.class);

        final FlowHandler flowHandler = innerFlow.createHandler(context);
        context.register("ContentFlowHandler", flowHandler);
        context.register("ContentFlow", innerFlow);
        final Duration containerAnimationDuration = Duration.millis(320);
        drawer.setContent(flowHandler.start(new AnimatedFlowContainer(containerAnimationDuration, SWIPE_LEFT)));
        context.register("ContentPane", drawer.getContent().get(0));

        // side controller will add links to the content flow
        Flow sideMenuFlow = new Flow(SideMenuController.class);
        final FlowHandler sideMenuFlowHandler = sideMenuFlow.createHandler(context);
        drawer.setSidePane(sideMenuFlowHandler.start(new AnimatedFlowContainer(containerAnimationDuration, SWIPE_LEFT)));

        btn1.setText("⧏");
        btn2.setText("▷");
        btn3.setText("⧐");
        btn4.setText("⟲"); // FIXME https://stackoverflow.com/questions/22885702/html-for-the-pause-symbol-in-a-video-control

        btn1.setOnAction(e -> Automaton.getInstance().restorePreviousState());
        btn2.setOnAction(e -> {
            if(Automaton.getInstance().isRunning()){
                Automaton.getInstance().stopCycleThread();
                btn2.setText("▷");
            } else {
                Automaton.getInstance().startCycleThread();
                btn2.setText("॥"); // stop automaton
            }
        });
        btn3.setOnAction(e -> Automaton.getInstance().doCycle());
        btn4.setOnAction(e -> {
            Automaton.getInstance().resetToFirstUserstate();

            /* FIXME
            FlowHandler contentFlowHandler = (FlowHandler) context.getRegisteredObject("ContentFlowHandler");
            try {
                contentFlowHandler.handle("automatonView");
            } catch (VetoException exc) {
                exc.printStackTrace();
            } catch (FlowException exc) {
                exc.printStackTrace();
            }
            */

            btn2.setText("▷");
        });

        btn5.setOnAction(e -> {
            Automaton.getInstance().resetToNew();
            FlowHandler contentFlowHandler = (FlowHandler) context.getRegisteredObject("ContentFlowHandler");
            try {
                contentFlowHandler.handle("automatonView");
            } catch (VetoException exc) {
                exc.printStackTrace();
            } catch (FlowException exc) {
                exc.printStackTrace();
            }
            update(null, null);
            btn2.setText("▷");
        });

        acceptButton.setOnMouseClicked((e) -> dialog.close());

        Automaton.getInstance().addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        lblCycle.setText("Cycle: " + Automaton.getInstance().getCurrentCycle());

        if(Automaton.getInstance().checkError()) {
            btn2.setText("▷");
            openDialogue();
        }
    }

    public void openDialogue() {
        dialog.setTransitionType(JFXDialog.DialogTransition.BOTTOM);
        dialog.show(root);
    }

    public static final class InputController {
        @FXML
        private JFXListView<?> toolbarPopupList;

        // close application
        @FXML
        private void submit() {
            if (toolbarPopupList.getSelectionModel().getSelectedIndex() == 1) {
                Platform.exit();
            }
        }
    }
}
