package main.java.de.jo2k.ca3.Controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.validation.ValidationFacade;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.util.StringConverter;
import main.java.de.jo2k.ca3.Model.Automaton;

import javax.annotation.PostConstruct;
import java.util.Objects;

@ViewController(value = "/main/resources/fxml/SideMenu.fxml", title = "xxx")
public class SideMenuController {

    @FXMLViewFlowContext
    private ViewFlowContext context;
    @FXML
    @ActionTrigger("automatonView")
    private Label automatonView;
    @FXML
    @ActionTrigger("celltypesView")
    private Label celltypesView;
    @FXML
    @ActionTrigger("rulesView")
    private Label rulesView;
    @FXML
    private JFXListView<Label> sideList;
    @FXML
    private JFXSlider speedSlider;

    @FXML
    private JFXComboBox<Label> jfxEditableComboBox;

    @FXML
    private JFXComboBox<Label> mapsize;

    public static String drawNext = "";

    /**
     * init fxml when loaded.
     */
    @PostConstruct
    public void init() {
        Objects.requireNonNull(context, "context");
        FlowHandler contentFlowHandler = (FlowHandler) context.getRegisteredObject("ContentFlowHandler");
        sideList.propagateMouseEventsToParent();
        sideList.getSelectionModel().selectedItemProperty().addListener((o, oldVal, newVal) -> {
            new Thread(()->{
                Platform.runLater(()->{
                    if (newVal != null) {
                        System.out.println(newVal.getText());

                        drawNext = newVal.getText();

                        /*
                        // FIXME menu switch
                        try {
                            contentFlowHandler.handle(newVal.getId());
                        } catch (VetoException exc) {
                            exc.printStackTrace();
                        } catch (FlowException exc) {
                            exc.printStackTrace();
                        }
                        */
                    }
                });
            }).start();
        });

        Flow contentFlow = (Flow) context.getRegisteredObject("ContentFlow");
        //bindNodeToController(automatonView, AutomatonViewController.class, contentFlow, contentFlowHandler);
        //bindNodeToController(celltypesView, CelltypesViewController.class, contentFlow, contentFlowHandler);
        //bindNodeToController(rulesView, RulesViewController.class, contentFlow, contentFlowHandler);

        Automaton.getInstance().setCycleThreadSpeed((int)speedSlider.getValue());
        speedSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                                Number old_val, Number new_val) {
                Automaton.getInstance().setCycleThreadSpeed(new_val.intValue());
            }
        });



        mapsize.setConverter(new StringConverter<Label>() {
            @Override
            public String toString(Label object) {
                return object==null? "" : object.getText();
            }

            @Override
            public Label fromString(String string) {
                String str = string.replaceAll("[^0-9]", "");
                if(string == "" || str == "") {
                    return new Label("");
                }

                int newSize = Integer.parseUnsignedInt(str);
                System.out.println(newSize);
                Automaton.getInstance().setSize(newSize);
                return new Label(str);
            }
        });




        ChangeListener<? super Boolean> comboBoxFocus = (o, oldVal, newVal) -> {
            if (!newVal) {
                ValidationFacade.validate(jfxEditableComboBox);
            }
        };
        // FIXME
        /*
        jfxEditableComboBox.focusedProperty().addListener(comboBoxFocus);
        jfxEditableComboBox.getEditor().focusedProperty().addListener(comboBoxFocus);
        jfxEditableComboBox.setConverter(new StringConverter<Label>() {
            @Override
            public String toString(Label object) {
                return object == null? "" : object.getText();
            }
            @Override
            public Label fromString(String string) {
                return new Label(string);
            }
        });
        */
    }

    private void bindNodeToController(Node node, Class<?> controllerClass, Flow flow, FlowHandler flowHandler) {
        flow.withGlobalLink(node.getId(), controllerClass);
    }

}
