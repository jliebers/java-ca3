package main.java.de.jo2k.ca3.Controller;

import io.datafx.controller.ViewController;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Scale;
import main.java.de.jo2k.ca3.Model.Automaton;
import main.java.de.jo2k.ca3.Model.AutomatonState;
import main.java.de.jo2k.ca3.Model.Cell;
import main.java.de.jo2k.ca3.View.CellRectangleGestures;
import main.java.de.jo2k.ca3.View.MapPane;
import main.java.de.jo2k.ca3.View.CellRectangle;
import main.java.de.jo2k.ca3.View.SceneGestures;

import javax.annotation.PostConstruct;
import java.util.Observable;
import java.util.Observer;

@ViewController(value = "/main/resources/fxml/Automaton.fxml", title = "AAA")
public class AutomatonViewController implements Observer {
    public final static int CELL_SIZE = 50; // size of each cell in pixels
    private MapPane canvas;

    int[] lastRedraw = {-1, -1};
    boolean lastDrag = false;

    @FXML
    private Pane pane;

    @PostConstruct
    public void init(){
        Group group = new Group();

        // create canvas
        canvas = new MapPane();


        // create sample nodes which can be dragged
        SceneGestures sceneGestures = new SceneGestures(canvas);
        pane.addEventFilter(MouseEvent.MOUSE_PRESSED, sceneGestures.getOnMousePressedEventHandler());
        pane.addEventFilter(MouseEvent.MOUSE_DRAGGED, sceneGestures.getOnMouseDraggedEventHandler());
        pane.addEventFilter(ScrollEvent.ANY, sceneGestures.getOnScrollEventHandler());

        // draw automat:
        // drawFun(canvas);
        // drawAutomaton(canvas, );
        drawAutomaton(Automaton.getInstance().getCurrentState());

        canvas.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            if(! e.getButton().name().equals("PRIMARY"))
                return;

            if (lastDrag){
                System.out.println("Dropped click by lastDrag");
                lastDrag = false;
                return;
            }

            System.out.println("CLICK "+e.getButton());
            System.out.println("CLICK "+e.getButton().name().equals("PRIMARY"));

            Node selectedNode = e.getPickResult().getIntersectedNode();

            if (selectedNode instanceof CellRectangle) {
                int x = ((CellRectangle) selectedNode).getCellX();
                int y = ((CellRectangle) selectedNode).getCellY();


                if(SideMenuController.drawNext.equals("")) {
                    Cell currentCell = Automaton.getInstance().getCurrentState().getCell(x, y);
                    if (currentCell.isAlive()) {
                        Automaton.getInstance().getCurrentState().setCell(x, y, new Cell(false));
                    } else {
                        Automaton.getInstance().getCurrentState().setCell(x, y, new Cell(true));
                    }
                } else {
                    switch(SideMenuController.drawNext){
                        case "Glider":
                            Automaton.getInstance().drawGOLGlider(x, y, Automaton.getInstance().getCurrentState());
                            break;
                        case "LW-Spaceship":
                            Automaton.getInstance().drawGOLLwSpaceship(x, y, Automaton.getInstance().getCurrentState());
                           break;
                        case "Tumbler":
                            Automaton.getInstance().drawGOLTumbler(x, y, Automaton.getInstance().getCurrentState());
                           break;
                        case "Octagon":
                            Automaton.getInstance().drawGOLOctagon(x, y, Automaton.getInstance().getCurrentState());
                           break;
                        case "Chain":
                            Automaton.getInstance().drawGOLChain(x, y, Automaton.getInstance().getCurrentState());
                           break;
                    }
                    SideMenuController.drawNext = "";
                }



                drawAutomaton(Automaton.getInstance().getCurrentState());
            }});

        canvas.addEventFilter(MouseEvent.MOUSE_DRAGGED, (MouseEvent e) -> { // Handles all mouseEvents which correspond to a cell
            if(! e.isPrimaryButtonDown()) {
                return; // we only handle PrimaryButton here
            }

            Node selectedNode = e.getPickResult().getIntersectedNode();

            if (selectedNode instanceof CellRectangle) {
                int x = ((CellRectangle) selectedNode).getCellX();
                int y = ((CellRectangle) selectedNode).getCellY();

                if(lastRedraw[0] == x && lastRedraw[1] == y){
                    return; // we have redrawn this cell during the last mouse movement, skip for now.
                }

                System.out.println("Node dragged: x:" + x + ", y:" + y);

                Cell currentCell = Automaton.getInstance().getCurrentState().getCell(x, y);

                if (currentCell.isAlive()) {
                    Automaton.getInstance().getCurrentState().setCell(x, y, new Cell(false));
                } else {
                    Automaton.getInstance().getCurrentState().setCell(x, y, new Cell(true));
                }

                lastRedraw[0] = x;
                lastRedraw[1] = y;
                lastDrag = true;
            }
            drawAutomaton(Automaton.getInstance().getCurrentState());
        });

        // rotate canvas so (0, 0) is at bottom left
        Scale scale = new Scale(1, -1);
        scale.pivotYProperty().bind(Bindings.createDoubleBinding(() ->
                pane.getBoundsInLocal().getMinY() + pane.getBoundsInLocal().getHeight() / 2, pane.boundsInLocalProperty())
        );
        pane.getTransforms().add(scale);


        // we don't want the canvas on the top/left in this example => just
        // translate it a bit

        canvas.setTranslateX(100);
        canvas.setTranslateY(100);

        Automaton.getInstance().addObserver(this);

        pane.getChildren().add(canvas);
        pane.toBack();
    }

    public void clearMap(){
        canvas.clear();
    }

    /**
     * This function draws the automaton.
     * It reads the provided AutomatonState in a loop and generates CellRectangles, which represent each Cell.
     * Each CellRectangle is also provided with an EventHandler CellRectangleGestures, which implements the clickable actions.
     * @param as
     */
    private void drawAutomaton(AutomatonState as){
        clearMap();

        for(int x = 0; x < as.getSize(); x++) {
            for(int y = 0; y < as.getSize(); y++){
                Cell cell = as.getCell(x, y);

                // Create a new CellRectangle, save x and y in it.
                CellRectangle cr = new CellRectangle(cell, x, y);

                canvas.drawCellRectangle(cr);
            }
        }
        drawGrid(canvas, Automaton.getInstance().getSize());
    }

    private void drawFun() {
        CellRectangleGestures cellRectangleGestures = new CellRectangleGestures(canvas);

        Label label1 = new Label("Draggable node 1");
        label1.setTranslateX(10);
        label1.setTranslateY(10);
        label1.addEventFilter( MouseEvent.MOUSE_PRESSED, cellRectangleGestures.getOnMousePressedEventHandler());
        label1.addEventFilter( MouseEvent.MOUSE_DRAGGED, cellRectangleGestures.getOnMouseDraggedEventHandler());

        Label label2 = new Label("Draggable node 2");
        label2.setTranslateX(100);
        label2.setTranslateY(100);
        label2.addEventFilter( MouseEvent.MOUSE_PRESSED, cellRectangleGestures.getOnMousePressedEventHandler());
        label2.addEventFilter( MouseEvent.MOUSE_DRAGGED, cellRectangleGestures.getOnMouseDraggedEventHandler());

        Label label3 = new Label("Draggable node 3");
        label3.setTranslateX(200);
        label3.setTranslateY(200);
        label3.addEventFilter( MouseEvent.MOUSE_PRESSED, cellRectangleGestures.getOnMousePressedEventHandler());
        label3.addEventFilter( MouseEvent.MOUSE_DRAGGED, cellRectangleGestures.getOnMouseDraggedEventHandler());

        Circle circle1 = new Circle( 300, 300, 50);
        circle1.setStroke(Color.ORANGE);
        circle1.setFill(Color.ORANGE.deriveColor(1, 1, 1, 0.5));
        circle1.addEventFilter( MouseEvent.MOUSE_PRESSED, cellRectangleGestures.getOnMousePressedEventHandler());
        circle1.addEventFilter( MouseEvent.MOUSE_DRAGGED, cellRectangleGestures.getOnMouseDraggedEventHandler());

        Rectangle rect1 = new Rectangle(100,100);
        rect1.setTranslateX(700);
        rect1.setTranslateY(700);
        rect1.setStroke(Color.BLUE);
        rect1.setFill(Color.BLUE.deriveColor(1, 1, 1, 0.5));
        rect1.addEventFilter(MouseEvent.MOUSE_PRESSED, cellRectangleGestures.getOnMousePressedEventHandler());
        rect1.addEventFilter(MouseEvent.MOUSE_DRAGGED, cellRectangleGestures.getOnMouseDraggedEventHandler());

        canvas.getChildren().addAll(label1, label2, label3, circle1, rect1);
    }

    /**
     * Add a grid to the canvas, send it to back
     */
    public void drawGrid(MapPane canvas, int size) {
        double w, h;
        w = h = size * CELL_SIZE;

        // add grid
        Canvas grid = new Canvas(w, h);

        // don't catch mouse events
        grid.setMouseTransparent(true);

        GraphicsContext gc = grid.getGraphicsContext2D();

        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);

        // draw grid lines
        double offset = 50;
        for(double i=0; i <= w; i+=offset) {
            gc.strokeLine(i, 0, i, h);
            gc.strokeLine(0, i, w, i);
        }

        canvas.getChildren().add(grid);
        grid.toBack();
    }

    @Override
    public void update(Observable o, Object arg) {
        drawAutomaton(Automaton.getInstance().getCurrentState());

        if(Automaton.getInstance().checkError()) { // check for error flag
            for (int[] uninitCell : Automaton.getInstance().getUnitializedCells()) {
                CellRectangle cr = new CellRectangle(null, uninitCell[0], uninitCell[1]);
                cr.blink(Color.RED);
                canvas.drawCellRectangle(cr);
            }
        }
    }
}